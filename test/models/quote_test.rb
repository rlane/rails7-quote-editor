require "test_helper"

class QuoteTest < ActiveSupport::TestCase
  setup do
    @company = companies(:kpmg)
  end

  test "valid quote" do
    quote = @company.quotes.build(name: 'test quote')
    assert quote.valid? 'quote with name should be valid'
  end

  test 'invalid without name' do
    quote = @company.quotes.build
    assert_not quote.valid? 'quote invalid without a name'
  end

  test "total_price returns the sum of the total_price of all line items" do
    assert_equal 2500, quotes(:first).total_price
  end
end
